# PHypervisor
A WORK IN PROGRESS:

PHypervisor  = DualSPHysics + (python hypervisor)

# What's a hypervisor?
Technically a hypervisor is computer software, firmware or hardware that creates and runs virtual machines. PHypervisor is a hypervisor for CFD experiments, so given a set of variables to explore, the hypervisor will run as many experiments as needed to satisfy the termination criteria (usually error).  As of now, we are only running random cases, spatially and temporally averaging results, and saving the results.  Next steps can be found in the issues.

It is expected that PHypervisor will live in same directory as DualSPHysics or any other programs that the PHypervisor might need access to (like Blender or Meshlab for geometry generation).

## Software Required
Blender 2.8+, DualSPHysics 4.4, Meshlab 2016+

All software locations can be specified in `applocations.py`

## Mandelbulb-in-a-box [Work in Progress]
This project is all about characterizing the Mandelbulb geometry space (defined by the exponent, iterations, and scale of the function defining the bulb).  Before running anything, review the script.

Start by 
`chmod +x ./linux64_CPU.sh`

If that is not your distro, please look at the modifications made to the associated script compared to the XXXdistro_CPUorGPU.sh/bat file needed for your machine. Please consider submitting a merge request if you do update the batch files.

Let's generate some cases:

`python 1_cases2run.py`

This will generate a npy file which will be referenced the next process:

`python 2_run_cases.py`

Each of the cases generated will be run.  First the geometry needed for the case will be generated using Blender (which looks at a temporary npy file with the correct bulb id to genereate).  Then DualSPHysics runs the case described in mandel_uniform_flow_Def.xml.  Finally the vtk files are summarized.  The process repeats, data is overwritten.  Only the geometry information and summaries are saved.

`python 3_check_results.py`

This checks to see if your results are different for each run (indicating things might be working).

There are more python files and and an jupyter notebook worth looking at if this example instersts you.

