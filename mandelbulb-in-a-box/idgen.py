import numpy as np

def idgen(Va,Vb,Vc,voxels,iters,frac_scale):
    bulbID = ''
    bulbID = str(('exp_'+str(Va)+
        '_'+str(Vb)+
        '_'+str(Vc)+
    '_res_'+str(voxels)+
    '_iters_'+str(iters)+
    '_frac_scale_'+str(frac_scale)
    ))
    return bulbID
