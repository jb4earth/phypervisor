#!/usr/bin/env python3
import bpy
from bpy import data, context
from mathutils import Matrix, Quaternion, Vector
from math import asin, atan2, cos, sin, sqrt, pi
from colorsys import hsv_to_rgb
from idgen import idgen
from applocations import meshlabserverlocation
import os
import random
import time
def append_obj_from_data(collection=context.collection,
                         name='Object',
                         obj_data=None,
                         rotation_mode='QUATERNION',
                         location=Vector((0.0, 0.0, 0.0)),
                         rotation=Quaternion((1.0, 0.0, 0.0, 0.0)),
                         scale=Vector((1.0, 1.0, 1.0)),
                         parent=None,
                         empty_display_size=0.5,
                         empty_display_type='PLAIN_AXES'):
    obj = data.objects.new(name, obj_data)
    obj.parent = parent
    set_obj_transform(obj=obj,
                      rotation_mode=rotation_mode,
                      location=location,
                      rotation=rotation,
                      scale=scale)
    # ['PLAIN_AXES', 'ARROWS', 'SINGLE_ARROW',
    #  'CIRCLE', 'CUBE', 'SPHERE', 'CONE', 'IMAGE']
    obj.empty_display_type = empty_display_type
    obj.empty_display_size = empty_display_size
    #print('finishing append_obj_from_data')
    collection.objects.link(obj)
    return obj

def set_obj_transform(obj=None,
                      rotation_mode='XYZ',
                      location=Vector((0.0, 0.0, 0.0)),
                      rotation=Quaternion((1.0, 0.0, 0.0, 0.0)),
                      scale=Vector((1.0, 1.0, 1.0))):
    obj.location = location
    obj.scale = scale

    # ['AXIS_ANGLE', 'QUATERNION',
    #  'XYZ', 'XZY', 'YXZ', 'YZX', 'ZXY', 'ZYX']
    obj.rotation_mode = rotation_mode

    obj.rotation_quaternion = rotation

    # To axis angle returns a tuple where the first value
    # is the axis; the second value, the angle.
    aa = rotation.to_axis_angle()
    obj.rotation_axis_angle[0] = aa[1]
    obj.rotation_axis_angle[1] = aa[0].x
    obj.rotation_axis_angle[2] = aa[0].y
    obj.rotation_axis_angle[3] = aa[0].z

    # To convert a Quaternion to Euler angles, the order
    # in which rotations are made must be known.
    if rotation_mode in ['XYZ', 'XZY', 'YXZ', 'YZX', 'ZXY', 'ZYX']:
        obj.rotation_euler = rotation.to_euler(rotation_mode)

    return obj

def gen_lattice(name='Lattice',
                collection=context.collection,
                resolution=(8, 8, 8),
                interp='KEY_BSPLINE',
                use_outside=False,
                rotation_mode='QUATERNION',
                location=Vector((0.0, 0.0, 0.0)),
                rotation=Quaternion((1.0, 0.0, 0.0, 0.0)),
                scale=Vector((1.0, 1.0, 1.0)),
                parent=None):
    l_dat = data.lattices.new(name)

    w_res = int(resolution[0])
    h_res = int(resolution[1])
    d_res = int(resolution[2])

    l_dat.points_u = w_res
    l_dat.points_v = h_res
    l_dat.points_w = d_res

    # ['KEY_LINEAR', 'KEY_CARDINAL', 'KEY_CATMULL_ROM', 'KEY_BSPLINE']
    l_dat.interpolation_type_u = interp
    l_dat.interpolation_type_v = interp
    l_dat.interpolation_type_w = interp

    l_dat.use_outside = use_outside

    lat_obj = append_obj_from_data(
        collection=collection,
        name=l_dat.name,
        obj_data=l_dat,
        rotation_mode=rotation_mode,
        location=location,
        rotation=rotation,
        scale=scale,
        parent=parent)
    return lat_obj

def vec_comp_mult(a=Vector(), b=Vector()):
    return Vector((
        a[0] * b[0],
        a[1] * b[1],
        a[2] * b[2]))

def float_lerp(a=0, b=1.0, t=0.5):
    # Unclamped.
    return (1.0 - t) * a + t * b


def gen_mobius_lat(name='Mobius Transform',
                   a=Vector((0.0, 0.0, 0.0)),
                   b=Vector((1.0, 1.0, 0.0)),
                   c=Vector((1.0, 1.0, 0.0)),
                   d=Vector((0.0, 0.0, 0.0)),
                   collection=context.collection,
                   resolution=(16, 16, 16),
                   interp='KEY_BSPLINE',
                   rotation_mode='QUATERNION',
                   location=Vector((0.0, 0.0, 0.0)),
                   rotation=Quaternion((1.0, 0.0, 0.0, 0.0)),
                   scale=Vector((1.0, 1.0, 1.0)),
                   parent=None):
    lat_obj = gen_lattice(
        name=name,
        collection=collection,
        resolution=resolution,
        interp=interp,
        use_outside=False,
        rotation_mode=rotation_mode,
        location=location,
        rotation=rotation,
        scale=scale,
        parent=parent)

    l_dat = lat_obj.data
    dimensions = Vector((
        0.5 * l_dat.points_u,
        0.5 * l_dat.points_v,
        0.0))
    points = l_dat.points
    for point in points:
        original = point.co
        coord = original.copy()
        if coord[0] != 0.0 and coord[1] != 0.0:
            coord = mobius_vec(
                a=a, b=b, c=c, d=d,
                z=coord)
            coord = vec_comp_mult(coord, dimensions)
            coord[2] = original[2]
            # coord[2] = original[2] * 0.75 + (coord[1] * coord[0] * 0.125)
            point.co_deform = coord

    return lat_obj

def mandelbrot_step(center=Vector(),
                    exponent=Vector((2.0, 0.0, 0.0)),
                    iterations=32,
                    exclude_upper=False):
    i = 0
    k = max(2, iterations)
    fac = 0.0

    # Convert vectors to complex numbers.
    c = complex(center[0], center[1])
    y = complex(exponent[0], exponent[1])
    z = c
    p = abs(z)

    iter_range = range(0, k, 1)
    for i in iter_range:

        # Raise zprev to the power of y,
        # add c, then assign to znext.
        z = (z ** y) + c

        # Find the length of z.
        p = abs(z)

        # If the length of z is greater
        # than 2, it will tend to infinity,
        # so quit the loop.
        if p > 2.0:
            break

    # If i reached the upper limit of iter_range
    # without escaping to infinity, then we
    # can fill in the fac or filter it out.
    if (not exclude_upper) or (i != k - 1):
        fac = i / (k - 1)

    # Convert z back to a vector.
    # Return a dictionary with strings as keys.
    return {'z': Vector((z.real, z.imag, 0.0)),
            'fac': fac,
            'abs': p,
            'c': center}


def mandelbrot(points=[],
               exponent=Vector((2.0, 0.0, 0.0)),
               iterations=32,
               exclude_upper=False):
    results = []
    for point in points:
        result = mandelbrot_step(
            center=point,
            exponent=exponent,
            iterations=iterations,
            exclude_upper=exclude_upper)
        results.append(result)

    return results

def gen_2d_grid(count=32,
                offset=Vector(),
                scale=1.0,
                min_corner=Vector((-0.5, -0.5, 0.0)),
                max_corner=Vector((0.5, 0.5, 0.0))):
    points = []
    count = max(count, 2)
    count_range = range(0, count, 1)
    count_to_percent = 1.0 / (count - 1)

    off_x = offset[0]
    off_y = offset[1]

    min_x = min_corner[0]
    min_y = min_corner[1]

    # Find width and height of grid.
    w = max_corner[0] - min_x
    h = max_corner[1] - min_y

    # Loop through rows.
    for i in count_range:
        i_percent = i * count_to_percent
        y = off_y + scale * (min_y + i_percent * h)

        # Loop through columns.
        for j in count_range:
            j_percent = j * count_to_percent
            x = off_x + scale * (min_x + j_percent * w)

            point = Vector((x, y, 0.0))
            points.append(point)

    return {'points': points,
            'width': w,
            'height': h}

def cube_faces(off=0):
    return [(off + 2, off + 0, off + 1, off + 3),
            (off + 6, off + 2, off + 3, off + 7),
            (off + 4, off + 6, off + 7, off + 5),
            (off + 0, off + 4, off + 5, off + 1),
            (off + 0, off + 2, off + 6, off + 4),
            (off + 5, off + 7, off + 3, off + 1)]

def cube_verts(pivot=Vector(), m=Matrix.Identity(4)):
    return [m @ (Vector((-0.5, -0.5, +0.5)) - pivot),
            m @ (Vector((-0.5, +0.5, +0.5)) - pivot),
            m @ (Vector((-0.5, -0.5, -0.5)) - pivot),
            m @ (Vector((-0.5, +0.5, -0.5)) - pivot),
            m @ (Vector((+0.5, -0.5, +0.5)) - pivot),
            m @ (Vector((+0.5, +0.5, +0.5)) - pivot),
            m @ (Vector((+0.5, -0.5, -0.5)) - pivot),
            m @ (Vector((+0.5, +0.5, -0.5)) - pivot)]

def non_uniform_scale(v=Vector((1.0, 1.0, 1.0))):
    return Matrix((
        Vector((v[0], 0.0, 0.0, 0.0)),
        Vector((0.0, v[1], 0.0, 0.0)),
        Vector((0.0, 0.0, v[2], 0.0)),
        Vector((0.0, 0.0, 0.0, 1.0))))

def visualize_fractal(collection=context.collection, name='Fractal',  material=None,  results=[],  fractal_center=Vector(),  fractal_scale=1.0, prune_cubes=True,  prune_lower_bound=0.01,  prune_upper_bound=1.0,  cube_pivot=Vector((0.0, 0.0, 0.0)), cube_size=Vector((1.0, 1.0, 1.0)),  min_cube_size=0.1, max_cube_size=0.9,  easing_func=float_lerp):
    cumulative_vertices = []
    cumulative_faces = []
    # Store offset for each cube and its faces.
    cube_index = 0
    face_index = 0
    verts_per_cube = 8
    inv_scale = 1.0 / fractal_scale

    for result in results:

        # Get data from the dictionary with a string keyword.
        fac = result['fac']

        # Do not add cubes where the factor is too low
        # or too high to be of interest. Both lower-
        # and upper-bounds are inclusive.
        too_low = fac < prune_lower_bound
        too_high = fac > prune_upper_bound
        if prune_cubes and (too_low or too_high):
            continue

        # Scale the cube by the mandelbrot function's fac.
        cube_scalar = easing_func(
            min_cube_size,
            max_cube_size,
            fac)

        # Undo fractal transformation.
        c_vec = result['c']
        c_vec = c_vec * inv_scale - fractal_center

        # Generate affine transform.
        trans_mat = Matrix.Translation(c_vec)
        scale_mat = non_uniform_scale(cube_scalar * cube_size)
        transform = trans_mat @ scale_mat

        # Append vertices and faces.
        cumulative_vertices += cube_verts(cube_pivot, transform)
        cumulative_faces += cube_faces(face_index)

        # Update cube index and face index.
        cube_index = cube_index + 1
        face_index = face_index + verts_per_cube

    # Create data from vertices and faces.
    # Edge data is not needed in this case.
    cube_data = data.meshes.new(name)
    cube_data.from_pydata(
        vertices=cumulative_vertices,
        edges=[],
        faces=cumulative_faces)
    cube_data.validate(
        verbose=False,
        clean_customdata=True)

    if material:
        cube_data.materials.append(material)
    #print('finishing visualize_fractal')
    return append_obj_from_data(
        name=name,
        collection=collection,
        obj_data=cube_data)



def float_smooth_step(a=0.0, b=1.0, t=0.5):
    # Clamped.
    if 0.0 >= t:
        return a
    if 1.0 <= t:
        return b
    s = t * t * (3.0 - 2.0 * t)
    return (1.0 - s) * a + s * b

def cube_mandelbrot(count=100,
                    collection=context.collection,
                    name='Mandelbrot',
                    material=None,
                    min_corner=Vector((-5, -5, 0.0)),
                    max_corner=Vector((5, 5, 0.0)),
                    exponent=Vector((2.0, 0.0, 0.0)),
                    iterations=32,
                    exclude_upper_mandel=False,
                    fractal_center=Vector((-0.375, 0.0, 0.0)),
                    fractal_scale=2.0,
                    prune_cubes=True,
                    prune_lower_bound=0.05,
                    prune_upper_bound=1.0,
                    min_cube_size=0.1,
                    max_cube_size=0.9,
                    cube_pivot=Vector((0.0, 0.0, -0.5)),
                    easing_func=float_lerp):
    grid = gen_2d_grid(
        count=count,
        offset=fractal_center,
        scale=fractal_scale,
        min_corner=min_corner,
        max_corner=max_corner)
    w = grid['width']
    h = grid['height']
    points = grid['points']
    dimensions = Vector((w, h, (w + h) * 0.5)) / max(count, 2)

    results = mandelbrot(
        points=points,
        exponent=exponent,
        iterations=iterations,
        exclude_upper=exclude_upper_mandel)
    #print('finishing cube_mandelbrot')
    return visualize_fractal(
        collection=collection,
        name=name,
        material=material,
        results=results,
        fractal_center=fractal_center,
        fractal_scale=fractal_scale,
        prune_cubes=prune_cubes,
        prune_lower_bound=prune_lower_bound,
        prune_upper_bound=prune_upper_bound,
        cube_pivot=cube_pivot,
        cube_size=dimensions,
        min_cube_size=min_cube_size,
        max_cube_size=max_cube_size,
        easing_func=easing_func)

def julia_step(seed=Vector((-0.8, 0.156, 0.0)),
               z=Vector(),
               exponent=Vector((2.0, 0.0, 0.0)),
               iterations=32,
               exclude_upper=False):

    # Seed in polar coordinates.
    # R: 0.8150680953147412
    # Phi: 2.9490093761298746

    i = 0
    k = max(2, iterations)
    fac = 0.0
    s = complex(seed[0], seed[1])
    y = complex(exponent[0], exponent[1])
    zn = complex(z[0], z[1])
    p = abs(zn)
    iter_range = range(0, k, 1)
    for i in iter_range:
        zn = (zn ** y) + s
        p = abs(zn)
        if p > 2.0:
            break
    if (not exclude_upper) or (i != k - 1):
        fac = i / (k - 1)
    return {'z': Vector((zn.real, zn.imag, 0.0)),
            'c': z,
            'fac': fac,
            'abs': p}

def cartesian(theta=0.0,
              phi=0.0,
              rho=1.0):
    rho_cos_phi = rho * cos(phi)
    return Vector((
        rho_cos_phi * cos(theta),
        rho_cos_phi * sin(theta),
        rho * -sin(phi)))

def mandelbulb_step(center=Vector((0.0, 0.0, 0.0)),
                    exponent=Vector((8.0, 8.0, 8.0)),
                    iterations=32,
                    exclude_upper=False):
    i = 0
    k = max(2, iterations)
    fac = 0.0
    z = center.copy()

    result = spherical(z)
    radius = result['rho']
    inclination = result['phi']
    azimuth = result['theta']

    # Cache exponent components.
    n0 = exponent[0]
    n1 = exponent[1]
    n2 = exponent[2]

    iter_range = range(0, int(k), 1)
    for i in iter_range:

        # Apply exponent to spherical coords.
#        #print(radius)
#        #print(n0)
#        #print(i)
        try:
            pn = (radius ** n0 ) # pow(radius, n0)
        except:
            pn = 100000000000000
#        #print(pn)
        nphi = n1 * inclination
        ntheta = n2 * azimuth

        # Convert to Cartesian coords, add center.
        z = cartesian(
            theta=ntheta,
            phi=nphi,
            rho=pn)
        z += center

        # Convert back to spherical.
        result = spherical(z)
        radius = result['rho']
        inclination = result['phi']
        azimuth = result['theta']

        if radius > 2.0:
            break

    if (not exclude_upper) or (i != k - 1):
        fac = i / (k - 1)

    return {'z': z,
            'fac': fac,
            'radius': radius,
            'inclination': inclination,
            'azimuth': azimuth,
            'c': center}

def spherical(v=Vector()):
    x = v[0]
    y = v[1]
    z = v[2]

    rho = x * x + y * y + z * z
    phi = 0.0
    if rho > 0.0:
        rho = rho ** 0.5  # or, sqrt(rho)

        # asin returns value in the range
        # [-pi / 2, pi / 2].
        phi = asin(z / rho)
    theta = atan2(y, x)
    return {'theta': theta,
            'phi': phi,
            'rho': rho}

def mandelbulb(points=[],
               exponent=Vector((8.0, 8.0, 8.0)),
               iterations=32,
               exclude_upper=False):
    results = []
    for point in points:
        result = mandelbulb_step(
            center=point,
            exponent=exponent,
            iterations=iterations,
            exclude_upper=exclude_upper)
        results.append(result)

    return results

def juliabulb(points=[],
               exponent=Vector((8.0, 8.0, 8.0)),
               iterations=32,
               exclude_upper=False):
    results = []
    for point in points:
        result = julia_step(
            center=point,
            exponent=exponent,
            iterations=iterations,
            exclude_upper=exclude_upper)
        results.append(result)

    return results

def gen_3d_grid(count=32,
                offset=Vector(),
                scale=1.0,
                min_corner=Vector((-0.5, -0.5, -0.5)),
                max_corner=Vector((0.5, 0.5, 0.5))):
    points = []
    count = max(count, 2)
    count_range = range(0, int(count), 1)
    count_to_percent = 1.0 / (count - 1)
    points = []

    off_x = offset[0]
    off_y = offset[1]
    off_z = offset[2]

    min_x = min_corner[0]
    min_y = min_corner[1]
    min_z = min_corner[2]

    # Find width, height and depth of grid.
    w = max_corner[0] - min_x
    h = max_corner[1] - min_y
    d = max_corner[2] - min_z

    # Loop through layers.
    for i in count_range:
        i_percent = i * count_to_percent
        z = off_z + scale * (min_z + i_percent * d)

        # Loop through rows.
        for j in count_range:
            j_percent = j * count_to_percent
            y = off_y + scale * (min_y + j_percent * h)

            # Loop through columns.
            for k in count_range:
                k_percent = k * count_to_percent
                x = off_x + scale * (min_x + k_percent * w)

                point = Vector((x, y, z))
                points.append(point)

    return {'points': points,
            'width': w,
            'height': h,
            'depth': d}

def cube_mandelbulb(count=12,
                    collection=context.collection,
                    name='Mandelbulb',
                    material=None,
                    min_corner=Vector((-0.5, -0.5, -0.5)),
                    max_corner=Vector((0.5, 0.5, 0.5)),
                    exponent=Vector((-3,-10,-500)),
                    iterations=32,
                    exclude_upper_mandel=False,
                    fractal_center=Vector(),
                    fractal_scale=2.0,
                    prune_cubes=True,
                    prune_lower_bound=0.5,
                    prune_upper_bound=1.0,
                    min_cube_size=1.00,
                    max_cube_size=1.00,
                    cube_pivot=Vector((0.0, 0.0, 0.0)),
                    easing_func=float_lerp):
    grid = gen_3d_grid(
        count=count,
        offset=fractal_center,
        scale=fractal_scale,
        min_corner=min_corner,
        max_corner=max_corner)
    w = grid['width']
    h = grid['height']
    d = grid['depth']
    points = grid['points']

    results = mandelbulb(
        points=points,
        exponent=exponent,
        iterations=iterations,
        exclude_upper=exclude_upper_mandel)
    geodef = np.zeros(len(results))
    for kk in np.arange(len(results)):
        geodef[kk] = results[kk]['fac']
#    #print(geodef)
#    #print('!!!!!!!!!!!!!!!!!!!!!!!!!!')
    dimensions = Vector((w, h, d)) / count
    obj = visualize_fractal(
        collection=collection,
        name=name,
        material=material,
        results=results,
        fractal_center=fractal_center,
        fractal_scale=fractal_scale,
        prune_cubes=prune_cubes,
        prune_lower_bound=prune_lower_bound,
        prune_upper_bound=prune_upper_bound,
        cube_pivot=cube_pivot,
        cube_size=dimensions,
        min_cube_size=min_cube_size,
        max_cube_size=max_cube_size,
        easing_func=easing_func)

    return obj, geodef

def lights_cameras_render():
        #context.scene.collection.objects.link(imported_object)
    # here we run meshlab to process the file.
    # 1 delete all faces
    # maybe merge dupliocate vertices
    # 2 remesh via screened poison reconstruction
    # 3 clean up via clean and repair -> remove isolated peices (7.8 10% were last known values)
    # 4 export to SAME stl (overwrite for storage savings
    #os.system('C:/"Program Files"/VCG/Meshlab/meshlabserver.exe -i C:/Users/john.brindley/Documents/geometries/fracidstring.obj -o C:/Users/john.brindley/Documents/geometries/delete_me.ply -o C:/Users/john.brindley/Documents/geometries/1.stl -s C:/Users/john.brindley/Documents/geometries/script-11.mlx')
#    #print('i got here yo ------------------')
#    break

    # lights cameras!
    tt = (sqrt(400)/2)**2
    locations = (tt, tt, tt)

    # create light datablock, set attributes
    light_data = bpy.data.lights.new(name="light_2.80", type='SUN')
    light_data.energy = 3

    ## create new object with our light datablock
    light_object = bpy.data.objects.new(name="light_2.80", object_data=light_data)

    ## link light object
    bpy.context.collection.objects.link(light_object)

    ## make it active
    bpy.context.view_layer.objects.active = light_object

    ##change location
    light_object.location = locations
    light_object.rotation_euler = (0,1.2,.2)
    #
    # create the first camera
    cam1 = bpy.data.cameras.new("camera")
    cam1.lens = 155

    # create the first camera object
    cam_obj1 = bpy.data.objects.new("camera", cam1)
    cam_obj1.location = locations
    cam_obj1.rotation_euler = (pi/3.3,0,3*pi/4)
    scn.collection.objects.link(cam_obj1)
    scene = bpy.context.scene
    scene.camera = cam_obj1
    path = bpy.path.abspath('//')

    scene.render.image_settings.file_format = 'PNG'
    scene.render.filepath = str((path + bulbID + '.png'))
    bpy.ops.render.render(write_still = 1)
    #print('iteration completed')

def remesh_poisson():
    path = bpy.path.abspath('//')

    # export object with its name as file name
    #fPath = str((path + 'fracidstring' + '.stl'))


    fP = str((path + 'temp' + '.stl'))

    #bpy.context.active_object = object
#    bpy.ops.export_mesh.stl(filepath=fPathobj)
    bpy.ops.export_mesh.stl(filepath=fP)
    print(os.getcwd())
    os.system(meshlablocation+' -i "'+(fP)+'" -o "'+(fP)+'"  -s poisson_n_delete.mlx')
    os.system(meshlablocation+' -i "'+(fP)+'" -o "'+(fP)+'"  -s r2.mlx')

    objs = [ob for ob in bpy.context.scene.objects]
    #print(objs)
    bpy.ops.object.delete({"selected_objects": objs})
    onject = bpy.ops.import_mesh.stl(filepath=fP,axis_up='Y')
    bpy.ops.object.origin_set(type="GEOMETRY_ORIGIN")


import numpy as np

def delete_all():
    objs = [ob for ob in bpy.context.scene.objects]
    #print(objs)
    bpy.ops.object.delete({"selected_objects": objs})

    objs = [ob for ob in bpy.data.materials]
    #print(objs)
    for item in objs:
        bpy.data.materials.remove(item)
    scn = bpy.context.scene


from math import trunc
def clean_geo(geo):
    # makes geostring also quantizes
    geo_string = ''
    for i in np.arange(len(geo)):
        if geo[i] > 0.5:
            geo[i] = int(1)
        else:
            geo[i] = int(0)
        #print(geo[i])
        geo_string = geo_string + str(trunc(geo[i]))
    return geo, geo_string

def quant2(data):
  for (x,y), value in np.ndenumerate(data):
    if value > 0.5:
      # #print(value - summary[y][1])
      data[x,y] = 1
    else:
      data[x,y] = 0
  return data

def calc_proxy(geo):
  res = geo.shape[1]
  siderot  = np.zeros((res,res))
  front = np.zeros((res,res))
  bottom = np.zeros((res,res))
  for i in range(0,res):
    front += geo[i,:,:]
    bottom += geo[:,i,:]
    siderot += geo[:,:,i]
  front_sum = np.sum(quant2(front))/(res*res)
  bottom_sum = np.sum(quant2(bottom))/(res*res)
  side_sum = np.sum(quant2(siderot))/(res*res)
  volume = np.sum(geo)/np.sum(np.ones((res,res,res)))
  return (front_sum,bottom_sum,side_sum,volume)

#def idgen(Va,Vb,Vc,voxels,iters,frac_scale):
    #bulbID = ''
    #bulbID = str(('exp_'+str(format(Va, '03d'))+
        #'_'+str(format(Vb, '03d'))+
        #'_'+str(format(Vc, '03d'))+
    #'_res_'+str(voxels)+
    #'_iters_'+str(iters)+
    #'_frac_scale_'+str(frac_scale)
    #))
    #return bulbID

def make_mandel(Va,Vb,Vc,voxels,iters,frac_scale):
    frac_scale.astype(int)
    object, geo = cube_mandelbulb(
                        count=voxels,
                        exponent=Vector((Va,Vb,Vc)),
                        iterations=iters,
                        fractal_scale=frac_scale
            )
    # set center and scale for meshlab
    object.location = (0,0,0)
    scale_by = 10
    object.scale = (scale_by, scale_by, scale_by)

    # send to meshlab
    remesh_poisson()

    # for database
    geo, geo_string = clean_geo(geo)
    bulbID = idgen(Va,Vb,Vc,voxels,iters,frac_scale)
    voxels = int(voxels)
    geo3 = geo.reshape((voxels,voxels,voxels)).transpose()
    #print(geo3.shape)
    front_sum,bottom_sum,side_sum,volume = calc_proxy(geo3)
    #TODO: delete the voxels that meshlab deleted....or maybe just don't show the geometry to the network??
    data = np.append((Va,Vb,Vc,voxels,iters,frac_scale,front_sum,bottom_sum,side_sum,volume),geo)

    header = ['Va','Vb','Vc','voxels','iters','frac_scale','front_sum','bottom_sum','side_sum','volume ','geo*216']

    #print(mandelid)
    np.save('npy/design_'+bulbID+'.npy', data)
    np.save('npy/geo_'+bulbID+'.npy', geo)


    # try:
    #     np.load('npy/design_header.npy')
    # except:
    np.save('npy/design_header.npy',header)
    return data, header, geo
