import pyvista as pv
import numpy as np
from decimal import *
from idgen import idgen
from loguru import logger
import glob

def getclipsizes(var_str,mesh,sensor_res):
    mesh.set_active_scalars(var_str)
    s_res = sensor_res
    ii = 0
    head = []
    nres = np.zeros((s_res*s_res,3))
    for j in np.arange(s_res):
        for k in np.arange(s_res):
            y = ((j/s_res)*2)-1
            z = ((k/s_res)*2)-1

            #'inlet'
            bounds = [-1,-0.8 , y,y+2/s_res, z,z+2/s_res]
            clipped = mesh.clip_box(bounds,invert=False)
            nres[ii][0] = clipped[var_str].size
            head.append('vol_inlet_' + str((j,k)))
#             print(clipped[var_str][2]**2)
            # dom
            bounds = [-0.8,0.8 , y,y+2/s_res, z,z+2/s_res]
            clipped = mesh.clip_box(bounds,invert=False)
            nres[ii][1] = clipped[var_str].size
            head.append('vol_domain_' + str((j,k)))

            # outlet
            bounds = [0.8,1 , y,y+2/s_res, z,z+2/s_res]
            clipped = mesh.clip_box(bounds,invert=False)
            nres[ii][2] = clipped[var_str].size
            head.append('vol_outlet_' + str((j,k)))
            ii +=1
    results = nres
    return results.flatten(), head


def clipndip(var_str,mesh,sensor_res):
    mesh.set_active_scalars(var_str)
    s_res = sensor_res
    ii = 0
    head = []
    nres = np.zeros((s_res*s_res,6))
    for j in np.arange(s_res):
        for k in np.arange(s_res):
            y = ((j/s_res)*2)-1
            z = ((k/s_res)*2)-1

            #'inlet'
            bounds = [-1,-0.8 , y,y+2/s_res, z,z+2/s_res]
            clipped = mesh.clip_box(bounds,invert=False)
            nres[ii][0] = (clipped[var_str]).mean()
            nres[ii][1] = clipped[var_str].std()
            head.append(var_str + '_inlet_mean_' + str((j,k)))
            head.append(var_str + '_inlet_std_' + str((j,k)))

            # dom
            bounds = [-0.8,0.8 , y,y+2/s_res, z,z+2/s_res]
            clipped = mesh.clip_box(bounds,invert=False)
            nres[ii][2] = (clipped[var_str]**2).mean()
            nres[ii][3] = clipped[var_str].std()
            head.append(var_str + '_domain_mean_' + str((j,k)))
            head.append(var_str + '_domain_std_' + str((j,k)))

            # outlet
            bounds = [0.8,1 , y,y+2/s_res, z,z+2/s_res]
            clipped = mesh.clip_box(bounds,invert=False)
            nres[ii][4] = (clipped[var_str]).mean()
            nres[ii][5] = clipped[var_str].std()
            head.append(var_str + '_outlet_mean_' + str((j,k)))
            head.append(var_str + '_outlet_std_' + str((j,k)))
            ii +=1
    results = nres
    return results.flatten(), head

def getclip(var_str,clipped,j,k,id):
    head = []
    nres = np.zeros(6)
    # TODO SQRING ERROR / OR WHY AM I SQRINH IN THE FIRST PLACE
    nres[0] = (abs(clipped[var_str][0])).mean() # squaring is problematic if the cell is empyty
    nres[1] = (abs(clipped[var_str][1])).mean()
    nres[2] = (abs(clipped[var_str][2])).mean()
    nres[3] = clipped[var_str][0].std()
    nres[4] = clipped[var_str][1].std()
    nres[5] = clipped[var_str][2].std()
    head.append(var_str + '_' + id + '_x_mean_' + str((j,k)))
    head.append(var_str + '_' + id + '_y_mean_' + str((j,k)))
    head.append(var_str + '_' + id + '_z_mean_' + str((j,k)))
    head.append(var_str + '_' + id + '_x_std_' + str((j,k)))
    head.append(var_str + '_' + id + '_y_std_' + str((j,k)))
    head.append(var_str + '_' + id + '_z_std_' + str((j,k)))
    return nres,head

def clipn3dip(var_str,mesh,sensor_res):
    mesh.set_active_scalars(var_str)
    s_res = sensor_res
    ii = 0
    header = []
    nres = np.zeros((s_res*s_res,18))
    for j in np.arange(s_res):
        for k in np.arange(s_res):
            y = ((j/s_res)*2)-1
            z = ((k/s_res)*2)-1

            #'inlet' ## changed the indexing to fix size?
            bounds = [-1,-0.8 , y,y+2/s_res, z,z+2/s_res]
            clipped = mesh.clip_box(bounds,invert=False)
            nres[ii][0:6],head = getclip(var_str,clipped,j,k,'inlet')
            header.append(head)
            # domain
            bounds = [-0.8,0.8 , y,y+2/s_res, z,z+2/s_res]
            clipped = mesh.clip_box(bounds,invert=False)
            nres[ii][6:12],head = getclip(var_str,clipped,j,k,'domain')
            header.append(head)

            # outlet
            bounds = [0.8,1 , y,y+2/s_res, z,z+2/s_res]
            clipped = mesh.clip_box(bounds,invert=False)
            nres[ii][12:18],head = getclip(var_str,clipped,j,k,'outlet')
            header.append(head)

            ii +=1
    results = nres
    flat_header = [item for sublist in header for item in sublist]
    return results.flatten(), flat_header

def get_lastfile(folder):
    list_of_files = glob.glob(folder) # * means all if need specific format then *.csv
    latest_file = max(list_of_files, key=os.path.getctime)
    logger.trace(latest_file)
    return latest_file

def get_xfiles(frames, folder):
    list_of_files = glob.glob(folder) # * means all if need specific format then *.csv
    return sorted(list_of_files)[-frames:]

def get_ting(results,result_header,varname,mesh):
    newdata, head = (clipndip(varname,mesh,3))
    results = np.hstack((results,newdata))
    result_header.append(head)
    logger.trace(head)
    return results, result_header

def analyze(*args):
    for arg in args:
        arg()

def surfaces(frames):
    files = get_xfiles(frames,'*/iso/*.vtk')
    for file in files:
        # mesh = pv.read(file)
        logger.trace(file)

def particles(frames):
    files = get_xfiles(frames,'*/particles/*.vtk')
    i = 0
    for file in files:
        logger.trace(file)
        result, header = particle(file)
        if i > 0:
            logger.trace(results.shape)
            results = np.vstack((results,result))
        else:
            results = result
        i += 1
        logger.trace('in particles ----!!!!!')
        logger.trace(header)
    return results, header

def analyze(frames, *args):
    header = []
    i = 0
    for arg in args:
        result, header = arg(frames)
        if i > 1:
            results = np.vstack((results,result))
        else:
            results = result
        i += 1
        # do something with the particles and or surfaces problem
    # mesh = pv.read(file)
    # f = np.ma.masked_greater(g,5)
    results = np.mean(results,axis=0)
    return results, header

def particle(file):
    logger.trace('PARTICLE(FILE) ++++++++++++++++++++++++')
    mesh = pv.read(file)
    result_header = []
    results, header = getclipsizes('Idp',mesh,3)
    result_header.append(header)
    logger.trace(result_header)
    # I don't want particle ids
    # newdata, header = (clipndip('Idp',mesh,3))
    # results = np.hstack((results,newdata))
    # result_header.append(header)
    # logger.trace(result_header)
    results, result_header = get_ting(results,result_header,'Rhop',mesh)
    results, result_header = get_ting(results,result_header,'Press',mesh)
    results, result_header = get_ting(results,result_header,'Vel',mesh)
    results, result_header = get_ting(results,result_header,'Vor',mesh)
    logger.trace(result_header)
    newdata, header = (clipn3dip('Vel',mesh,3))
    results = np.hstack((results,newdata))
    result_header.append(header)
    newdata, header = (clipn3dip('Vor',mesh,3))
    results = np.hstack((results,newdata))
    result_header.append(header)
     # results.shape
    return results, result_header
