import glob
import numpy as np
from loguru import logger
import sys

# logger.remove()

logger.add(sys.stderr, level="SUCCESS")
logger.add(sys.stderr, level="DEBUG")

def get_result_attributes(list_of_files):
    maybe_break = 0
    i = 0
    sums = np.zeros(len(list_of_files))
    means = sums
    stds = sums
    for file in list_of_files:
        logger.trace(file)
        data = (np.load(file))
        logger.trace(data)
        sums[i] = data.sum()
        means[i] = data.mean()
        stds[i] = data.std()
        try:
            if sums[i] - sums[i-1] == 0:
                logger.trace('sums equal for '+str(i))
                maybe_break += 1
            if means[i] - means[i-1] == 0:
                logger.trace('means equal for '+str(i))
                maybe_break += 1
            if stds[i] - stds[i-1] == 0:
                logger.trace('stds equal for '+str(i))
                maybe_break += 1
        except:
            logger.trace('loop one in list_of_files')
        i += 1
    fail_rate = (maybe_break/3)/(len(list_of_files)-1)*100
    return sums, means, stds, fail_rate

# print(n3 - n2)

def check_data():
    rlist_of_files = glob.glob('npy/result_e*.npy')
    rsums, rmeans, rstds, sim_rate = get_result_attributes(rlist_of_files)
    logger.success('number of samples: '+str((rsums.shape)))
    logger.success('result_sim_rate:'+str(sim_rate))
    if sim_rate > 10:
        logger.critical('result similarity rate is high! value = '+str(sim_rate)+'%')
    dlist_of_files = glob.glob('npy/design_e*.npy')
    dsums, dmeans, dstds, sim_rate  = get_result_attributes(dlist_of_files)
    logger.success('design_sim_rate:'+str(sim_rate))
    if sim_rate > 10:
        logger.critical('design similarity rate is high! value = '+str(sim_rate)+'%')
    glist_of_files = glob.glob('npy/geo_e*.npy')
    sums, means, stds, sim_rate = get_result_attributes(glist_of_files)
    logger.success('geo_sim_rate:'+str(sim_rate))
    if sim_rate > 10:
        logger.critical('geometry similarity rate is high! value = '+str(sim_rate)+'%')

check_data()
