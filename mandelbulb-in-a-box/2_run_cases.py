
#!/usr/bin/env python3

import os
from os import path
import sys
import numpy as np
import glob
import subprocess
import sys
from loguru import logger
import shutil
from time import sleep, perf_counter, ctime, time

from applocations import blenderlocation, dualsphlocation, replace_line

sys.path.append(".")
from analysis import *
from progress.bar import FillingSquaresBar

logger.remove()
logger.add(sys.stderr, level="CRITICAL")

# set directories in applocations

replace_line('linux64_GPU.sh',16,'export dirbin='+dualsphlocation)
replace_line('linux64_CPU.sh',16,'export dirbin='+dualsphlocation)



def run_dualsph():
    logger.trace('running dualsph')
    dualsph = './linux64_GPU.sh'
    dsph = subprocess.run(dualsph,shell=True,check=True,stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    logger.trace(dsph.stdout)
    logger.debug('dualsph run complete')
    bar.next()

def blend_mesh():
    # hacked together blender and meshlab code
    rm_file('temp.stl')
    logger.trace('running blender')
    blender_run_com = blenderlocation + ' --background --python create_mandel.py'
    blend = subprocess.run(blender_run_com,shell=True,check=True,stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    logger.trace(blend.stdout)
    logger.debug('blender stl generation complete')
    bar.next()


def analyze_vtks(frames):
    # rm_file('result_header.npy')
    results, head = analyze(frames, particles) # add surfaces
    logger.trace(results.shape)
    np.save('result_header.npy',head)
    np.save('npy/result_'+bulbid+'.npy',results)
    logger.trace(head)
    logger.trace(len(head))
    logger.debug('results saved')
    bar.next()



def rm_file(myfile):
    try:
        os.remove(myfile)
        logger.debug(myfile + ' = deleted')
    except:  ## if failed, report it back to the user ##
        logger.debug(myfile + ' = does not exist to be deleted')

# this is where the cases come from!
geos2run = np.load('npy/geos1.npy')

n_runs = geos2run.shape[0]
print(n_runs)
bar = FillingSquaresBar(max = 3*n_runs)

# let's look at the multi-processing pool instead of running in a dumb for loop

i = 0

for geo in geos2run:
## Create geometry describor (bulb ID)
    start = perf_counter()
    rm_file('npy/temp_geo.npy')
    logger.debug(str((geo[0],geo[1],geo[2],geo[3],geo[4],geo[5])))
    bulbid = idgen(geo[0],geo[1],geo[2],geo[3],geo[4],geo[5])
    np.save('npy/temp_geo.npy', geo)
    logger.trace(geo)

## Create STL
    blend_mesh()
## Run DualSPHysics
    run_dualsph()

## Analyze the Case (grab the last vtk and analyze)

    analyze_vtks(10)
    stop = perf_counter()
    i += 1
    remaining = ctime(time()+(n_runs*(stop-start)*(1-i/n_runs)))
    logger.critical('time remaining: ' + str(remaining))

bar.finish()

# show what we've run
# list_of_files = glob.glob('processed/*.vtk')
# logger.success(list_of_files)

# remove some files we don't need
# rm_file('npy/temp_geo.npy')
# rm_file('temp.stl')
