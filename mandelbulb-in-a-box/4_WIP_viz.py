# analyze results Files

import glob
import numpy as np
from loguru import logger
import sys
import matplotlib.pyplot as plt
# logger.remove()

logger.add(sys.stderr, level="SUCCESS")
logger.add(sys.stderr, level="DEBUG")


rlf = glob.glob('npy/result_e*.npy')
dlf = glob.glob('npy/design_e*.npy')
glf = glob.glob('npy/geo_e*.npy')

# print((len(rlf),len(dlf),len(glf)))
def length_check(rlf,dlf,glf):
    if len(rlf) != len(dlf):
        logger.critical('arrays are not the same length (result and design)')
    if len(rlf) != len(glf):
        logger.critical('arrays are not the same length (result and geo)')
    if len(dlf) != len(glf):
        logger.critical('arrays are not the same length (design and geo)')

length_check(rlf,dlf,glf)

data = np.zeros((len(dlf),2))
for i in np.arange(len(dlf)):
    res = (np.load(rlf[i]))
    des = (np.load(dlf[i]))
    geo = (np.load(glf[i]))
    data[i][0] = geo.mean()
    data[i][1] = res.mean()

print(data)
