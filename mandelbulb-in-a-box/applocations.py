meshlabserverlocation = '../../experiments/meshlab/MeshLabServer2020.04-linux.AppImage'
blenderlocation = '../../experiments/blender-2.80-linux-glibc217-x86_64/blender'
dualsphlocation='../../experiments/DualSPHysics_v4.4_053/bin/linux'
# for DualSPHysics see ./linux64_GPU or CPU line 17

def replace_line(fname,line,text):
    lines = open(fname,'r').readlines()
    lines[line] = text+'\n'
    out = open(fname,'w')
    out.writelines(lines)
    out.close
