import numpy as np
import random
import sys
from loguru import logger
# from decimal import *
# geos = np.load('geos0.npy')
# logger.remove()

# logger.add(sys.stderr, level="TRACE")

# print(geos)
cases = 1000
g2 = np.zeros((cases,6))
for i in np.arange(cases):
    random.seed(i+7000)
    j = random.randrange(-100,100)/10
    k = random.randrange(-100,100)/10
    l = random.randrange(-100,100)/10
    # l = -10
    # j = -10
    # k = -10
    logger.trace((j,k,l))
    g2[i] = np.array([(j),(k),(l), int(6), int(1000), int(2)])

# print(g2)
# geos = np.vstack((geos,g2))
# print(geos)
logger.debug(g2)
np.save('npy/geos1.npy',g2)
